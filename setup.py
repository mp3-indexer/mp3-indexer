from setuptools import setup, find_packages
import os

version = '0.1.0'
long_description = open("README.txt").read()
classifiers = [
	"Programming Language :: Python",
	("Topic :: Software development ::"
	"Libraries :: Python Modules")]

setup(name='mp3db',
	version=version,
	description='Indexes all the mp3 files in the system',
	long_description=long_description,
	classifiers = classifiers,
	keywords = 'sramsubu@gmail.com',
	url='https://gitorious.org/mp3-indexer',
	license= 'GPL'
	packages=find_packages(exclude=['ez_setup']),
	namespace_packages=['mp3db'],
	include_package_data=True,
	install_requires=[
	'setuptools',
	#-*- Extra requirements: -*-
	],
	test_suite='nose.collector',
	entry_points="""
	#-*- Entry points: -*-
	""",
	)
